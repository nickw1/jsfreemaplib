const Dialog = require('./Dialog');

function Nominatim(options) {
          const searchDlg = new Dialog(options.parent||document.body,
                     {'OK': ()=> { searchDlg.hide(); }},
                    { top: '100px', left: '100px', width: '200px',
                     position: 'absolute',
                        fontSize: '80%',
                    backgroundColor: 'white',
                    color: 'black',
                    padding: '5px',
                    borderRadius: '5px',
                    border: '1px solid black'});
        document.getElementById(options.searchBtn||'searchBtn').addEventListener('click', e=> {
            const q = document.getElementById(options.searchField||'q').value;
            fetch(options.url.replace('{q}', q)).then(response=>response.json()).then(json=> {
                const nodes = json.filter(o => o.lat !== undefined && o.lon !== undefined);
                if(nodes.length==0) {
                    alert(`No results for '${q}'`);
                } else {
                    searchDlg.show();
                    const div = document.createElement("div");
                    const h2=document.createElement("h2");
                    h2.appendChild(document.createTextNode("Search results"));
                    div.appendChild(h2);
                    nodes.forEach(o=> {
                        const p = document.createElement("p");
                        const a = document.createElement("a");
                        a.href='#';
                        a.innerHTML = o.display_name;
                        a.addEventListener("click", e=> {
                            if(options.onPlaceSelected) {
                                options.onPlaceSelected(o.lon, o.lat);
                            }
                            searchDlg.hide();
                        });
                        p.appendChild(a);
                        div.appendChild(p);
                    } );
                    searchDlg.setDOMContent(div);
                }
            } )
        } );
}

module.exports = Nominatim;
