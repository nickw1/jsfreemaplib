/*
 * DemApplier
 * 
 * Applies a given DEM tile to a given OSM tile.
 * 
 * Adds the elevation of each point in a way in metres as index 2 of the 
 * geometry.
 */

class DemApplier {

    constructor(demTiler, osmTiler) {
        this.demTiler = demTiler;
        this.osmTiler = osmTiler;
    }

    /*
     * update()
     *
     * call when we get a new position
     *
     * Calls update() on the DEM and OSM tilers to get arrays of new tiles, and
     * then applies each DEM tile to the corresponding OSM tile.
     *
     * Returns array of updated OSM tiles with elevation added as a third 
     * member of the coordinates of each point.
     */

    async update(p) {
        p = this.demTiler.sphMerc.project(p[0], p[1]);
        const demTiles = await this.demTiler.update(p);
        const osmTiles = await this.osmTiler.update(p);

        demTiles.forEach ( (demTile,j) => { 
             osmTiles[j].data.features.forEach  ( (f,i)=> {
                const line = [];
                if(f.geometry.type=='LineString' && f.geometry.coordinates.length >= 2) {
                    f.geometry.coordinates.forEach (coord=> {
                        const projCoord = this.demTiler.sphMerc.project(coord[0], coord[1]);
                        const h = demTile.data ? demTile.data.getHeight(projCoord[0], projCoord[1]) : 0;
                        if (h > Number.NEGATIVE_INFINITY) {
                            coord[2] = h; // raw geojson will contain elevations
                        }
                       });
                } else if(f.geometry.type == 'Point') {
                    const projCoord = this.demTiler.sphMerc.project(f.geometry.coordinates[0], f.geometry.coordinates[1]);
                    const h = demTile.data ? demTile.data.getHeight(projCoord[0], projCoord[1]) : 0;
                    if(h > Number.NEGATIVE_INFINITY) {
                        f.geometry.coordinates[2] = h;
                    }
                }
            });
        });
        return this.osmTiler.getCurrentTiles();
    }
}

module.exports = DemApplier;
