const GoogleProjection = require('./GoogleProjection');
const Tiler = require('./Tiler');
const Tile = require('./Tile'); 
const BoundingBox = require('./BoundingBox');
const Dialog = require('./Dialog');
const Nominatim = require('./Nominatim');
const JsonTiler = require('./JsonTiler');
const DEM = require('./DEM');

module.exports = {
    GoogleProjection: GoogleProjection,
    Tiler: Tiler,
    Tile: Tile,
    JsonTiler: JsonTiler,
    DEM: DEM,
    BoundingBox: BoundingBox,
    Dialog: Dialog,
    Nominatim: Nominatim,
    getBoundingBox : function(coords) {
        var bbox = new BoundingBox(181, 91, -181, -91);
        coords.forEach( p1 => {
            var p = [p1[0], p1[1]];
            if(p[0] < bbox.bottomLeft.x) {
                bbox.bottomLeft.x = p[0];
            }
            if(p[1] < bbox.bottomLeft.y) {
                bbox.bottomLeft.y = p[1];
            }
            if(p[0] > bbox.topRight.x) {
                bbox.topRight.x = p[0];
            }
            if(p[1] > bbox.topRight.y) {
                bbox.topRight.y = p[1];
            }
        });
        return bbox;
    },

    dist: function(x1,y1,x2,y2) {
        var dx = x2-x1, dy = y2-y1;
        return Math.sqrt(dx*dx + dy*dy);
    },

    // from old osmeditor2 code - comments as follows:     
    // find the distance from a point to a line     
    // based on theory at:     
    // astronomy.swin.edu.au/~pbourke/geometry/pointline/     
    // given equation was proven starting with dot product     

    // Now returns an object containing the distance, the intersection point 
    //and the proportion, in case we need these

    haversineDistToLine: function (x, y, p1, p2)  {         
        var u = ((x-p1[0])*(p2[0]-p1[0])+(y-p1[1])*(p2[1]-p1[1])) / (Math.pow(p2[0]-p1[0],2)+Math.pow(p2[1]-p1[1],2));        
 
        var xintersection = p1[0]+u*(p2[0]-p1[0]), yintersection=p1[1]+u*(p2[1]-p1[1]);   
        return (u>=0&&u<=1) ? {distance: this.haversineDist(x,y,xintersection,yintersection), intersection: [xintersection, yintersection], proportion:u} : null;
    },     

    haversineDist: function  (lon1, lat1, lon2, lat2)    {            
        var R = 6371000;            
        var dlon=(lon2-lon1)*(Math.PI / 180);            
        var dlat=(lat2-lat1)*(Math.PI / 180);            
        var slat=Math.sin(dlat/2);            
        var slon=Math.sin(dlon/2);            
        var a = slat*slat + Math.cos(lat1*(Math.PI/180))*Math.cos(lat2*(Math.PI/180))*slon*slon;            
        var c = 2 *Math.asin(Math.min(1,Math.sqrt(a)));            
        return R*c;        
    }
};
