jsfreemaplib
============

Miscellaneous JavaScript functions and classes for working with (primarily OpenStreetMap) geodata, including functions for working with Spherical Mercator projection, managing XYZ-tiled data and downloading and using Terrarium DEM data. Also one or two UI classes such as a Dialog.

versions 0.4.x
--------------

Version 0.4.2 - non breaking change - added DemApplier to apply DEM data to GeoJSON data.

Version 0.4.0 introduced one significant change, in the `DEM` class, if
`getHeight()` cannot find an elevation in the DEM, it will return 
`Number.NEGATIVE_INFINITY` rather than -1, to allow for below-sea-level
elevations.


versions 0.3.x
--------------

Versions 0.3.x are not fully compatible with the 0.2.x series. In particular
there are changes to the Tiler and DEM classes, see the source code for detail.
The other classes are unaffected.

A new DemTiler class has been added to handle tiled DEM data in Terrarium
format.

Note that `aframe-osm-3d` version 0.1.x is not compatible with jsfreemaplib
0.3.x. You must continue to use 0.2.x. `aframe-osm-3d` will be updated to
be jsfreemaplib 0.3.x compatible in due course.
